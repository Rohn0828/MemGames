import random
from threading import Thread, Event, Timer
import os
import time

class gameRunner():
    def __init__(self):
        self.rowValue = self.columnValue = int(4)
        self.numOfAttr = (self.rowValue * self.columnValue)
        self.pairOfPictures = int(self.numOfAttr / 2)
        self.gameStage = 0
        self.gameScore = 0
        self.gameCombo = 0
        self.gameTime = 0
        self.picRowList = list()
        self.picList = list()
        self.gameAttr = dict()
        self.gameAttr["value"] = list()
        self.gameAttr["cnt"] = list()
        for i in range(0, self.pairOfPictures):
            self.gameAttr["cnt"].append(2)


        ## Create 2D Array for MAP
        for x in range(0, self.rowValue):
            for y in range(0, self.columnValue):
                self.picRowList.append(255)
            self.picList.append(self.picRowList)
            self.picRowList = []
            print(self.picList[x])

    def cls(self):
        # clear screen
        os.system('cls')

    def randomGenerate(self, rangelist, num, duplicate):
        result = list()
        if duplicate :
            # it can return duplicated value
            for i in range(0, num):
                temp = random.choice(rangelist)
                result.append(temp)
        else:
            # it picks each value (not duplicate)
            result = random.sample(rangelist, num)
        return result

    def setGame(self):
        self.gameAttr["value"] = self.randomGenerate(range(0, self.pairOfPictures), self.pairOfPictures, False)
        gameAttr = self.gameAttr

        for x in range(0, self.rowValue):
            for y in range(0, self.columnValue):
                while True:
                    value = self.randomGenerate(gameAttr["value"], 1, False)
                    if gameAttr["cnt"][value[0]] > 0:
                        self.picList[x][y] = value
                        gameAttr["cnt"][value[0]] = gameAttr["cnt"][value[0]] - 1
                        #print(self.picList[x][y] )
                        break
                    else :
                        gameAttr["value"].remove(value[0])

    def gameMapPrintOut(self):
        ## Create 2D Array for MAP
        for x in range(0, self.rowValue):
            #for y in range(0, self.columnValue):
            time.sleep(1)
            print("\t", self.picList[x])

    def gameMapHidePrintOut(self):
        time.sleep(1)
        self.cls()
        hideAttr = [["X","X","X","X"],["X","X","X","X"],["X","X","X","X"],["X","X","X","X"]]
        hideAttrlow = list()
        for x in range(0, self.rowValue):
            print("\t", hideAttr[x])


run = gameRunner()
run.cls()
run.setGame()
run.gameMapPrintOut()
run.gameMapHidePrintOut()

